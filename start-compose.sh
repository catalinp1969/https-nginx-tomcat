#!/bin/bash
# This startup script is needed, because otherwise
# the ip address of the backend in the nginx.conf
# will be wrong! For details, read Readme.md!
IP=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" \
     | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1)
echo $IP
sed -i -e "s/BACKENDIP/$IP/g" nginx/conf/nginx.conf

docker-compose up --build -d
